

resource "kubernetes_service" "service" {
  metadata {
    name = "tamedia-service"
    namespace = "default"
    
    annotiations{
        "cloud.google.com/app-protocols" = "{'my-https-port':'HTTPS','my-http-port':'HTTP'}"
    }

    labels {
      service  = "tamedia-service"
      
    }
  }

  spec {
    selector {
      service  = "tamedia-service"
    }

    type = "NodePort"

    port {
      port = 80
      target_port = 11130
    }
  }
}