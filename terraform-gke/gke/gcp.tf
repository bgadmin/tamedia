provider "google" {
  credentials = "${file("../../creds/terraform-tamedia-task.json")}"
  project = "${var.project}"
  region  = "${var.region}"
}