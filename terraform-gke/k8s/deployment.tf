resource "kubernetes_deployment" "example" {
  metadata {
    name = "tamedia"
  }

  spec {
    replicas = 3
    min_ready_seconds = 5

    selector {
      match_labels {
        service = "tamedia-service"
      }
    }

    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge = 1
        max_unavailable = 0
      }
      
    }
    template {
      metadata {
        labels {
          service = "tamedia-service"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/bgadmin/tamedia:latest"
          name  = "tamedia"
          port {
            container_port = 11130
          }
          readiness_probe
          http_get {
            path = "/"
            port = 11130

          }
          initial_delay_seconds = 5
          period_seconds = 5
          success_threshold = 3

            }
          }
        }
      }
}