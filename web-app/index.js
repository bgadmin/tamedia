const http = require('http')
const port = 11130

const requestHandler = (request, response) => {
  console.log(request.url)
  response.end('Hello Tamedia from @Bojan Radulovic')
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
