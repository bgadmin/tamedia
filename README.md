# TAMEDIA DEVOPS TASK

### Prerequisites:

1. Google Cloud Account
2. GitLab Account
3. Docker
4. gcloud-sdk with kubernetes addon 
5. Terraform

### Content

```
├── README.md
├── creds (this should be encrypted)
├── k8s (all k8s files)
├── new-key.json
├── node_modules
├── package-lock.json
├── terraform-gke
├── terraform.tfstate
├── terraform.tfstate.backup
└── web-app
```

### GitLab
1. Create project tamedia
2. Configure required environemnt variables in Settiings -> CI/CD -> Environment variables to hide secrets from git (DOCKER_USER, DOCKER_PASS)
3. Create docker registry for images described in .gitlab.ci.yml
4. Add .gitlab-ci.yml file in the root of the project
5. Sample nodejs web application running on port 11130 with desired hello line
6. Dockerfile that runs nodejs web app with pm2-runtime
7. .gitignore creds dir that stores credentials for terraform

### Google Cloud
1. Create project tamedia-task
2. Create service accounts with following permissions:
- GitLab CI  
  - Kubernetes Engine Admin
  - Kubernetes Engine Developer
  - Editor
  - Storage Object Admin
- Terraform 
  - Kubernetes Engine Admin
  - Editor 
3. Authenticate to gcloud from command line
4. Secrets:
 - Configure secret in k8s to access private docker registry on gitlab https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/ 
5. Create kubernetes cluster
6. Create deployment
7. Create service
8. Create ingress 
9. Create self signed certificate and add in k8s secrets 

```
kubectl create secret tls tamedia-secret --key host.key --cert host.crt
```
10. Implement auto ssl
https://docs.cert-manager.io/en/latest/tasks/issuers/setup-ca.html


10. Static IP fro ingress: 
https://cloud.google.com/kubernetes-engine/docs/tutorials/configuring-domain-name-static-ip


### Domain
1. Create account on freenom.com 
2. Create domain tamedia-bojan.tk and point A record to ingress endpoint

### Terraform 
1. Create kubernetes resources specified in terraform

### TODO:
1. Create remote place to store tfstate file

### Drawbacks and notes
0. Certificates must be valid because issuer is now showing this error and in browser doesnt load https properly

```
Error getting keypair for CA issuer: certificate is not a CA
```
1. GitLab step 3. there was a problem with key.json downloaded from google cloud which made it impossible to authenticate service account, I've tried with different images etc just to lose time, new key solved the problem. 

2. We chose gcloud and gitlab since they got nice services such as kubernetes and CI/CD that integrate well with one another and for learning purposes ince I've never used them.

3. We chose pm2-runtime which gives production level process management for nodejs web app.

4. Terraform lacks ingress resource so we are stuck with parital terraform and creating resources by hand with gcloud cli
https://github.com/terraform-providers/terraform-provider-kubernetes/issues/14

5. Terraform written, didn't test it only cluster part / other k8s resources crewated by gcloud cli

